import 'dart:math';

import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget{
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

var SetTextStyle = const TextStyle(fontWeight: FontWeight.bold,fontSize: 32,color: Colors.white);

class _MyHomePageState extends State<MyHomePage>{

  //NumPad
  List<String> NumPad = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'C',
    '0',
    'OK',
  ];
  int num1 = 1;
  int num2 = 2;
  int score = 0;

  //input
  String input = '';

  //buttonPressFunction
  void buttonPressFunction(String button){
    setState(() {
      if(button == "OK"){
        CheckAns();
      }
      else if(button == 'C'){
        input = '';
      }
      else if(input.length < 3){
        input += button;
      }
    });
  }
  
  void CheckAns(){
    if(num1+num2 == int.parse(input)){
      score++;
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Color.fromARGB(255, 114, 8, 96),
          content: Container(height: 200,
          color: Color.fromARGB(255, 114, 8, 96),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Correct!',style: SetTextStyle,),
            Text('Score: ' + score.toString(),style: SetTextStyle,),
            Center(
              child: GestureDetector(
                onTap: Continued,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.arrow_circle_right,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      });
    }else{
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Color.fromARGB(255, 114, 8, 96),
          content: Container(height: 200,
          color: Color.fromARGB(255, 114, 8, 96),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Incorrect!',style: SetTextStyle,),
            Center(
              child: GestureDetector(
                onTap: Continued,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.arrow_circle_right,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      }
      );
      score = 0;
    }
  }

  var randomNum = Random();

  void Continued(){
    Navigator.of(context).pop();

    setState(() {
      input = '';
    });

    num1 = randomNum.nextInt(10);
    num2 = randomNum.nextInt(10);
  }


  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 208, 13, 176),
      body: Column(
        children: [
          Container(
            height: 50,
            color: Color.fromARGB(255, 156, 12, 115),
            child: Text('Score: '+ score.toString(),style: SetTextStyle,),
          ),
          //Quiz
          Expanded(
            child: Container(
              child: Center(
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text(num1.toString() + ' + ' + num2.toString() + ' = ',style: SetTextStyle),
                    //input
                    Container(
                      height: 50,
                      width: 100,
                      color: Color.fromARGB(255, 114, 8, 96),
                      child: Center(
                        child: Text(input, style: SetTextStyle),
                      ), 
                    ),
                   
                    ],
                  ),
                ),
              ),
            ),
            //pad
            Expanded(
            flex: 3,
            // child: Container(
            //   color: Color.fromARGB(255, 114, 8, 96),
            // ),
            child:Padding(
              padding: const EdgeInsets.all(10.0),
              child: GridView.builder(itemCount: NumPad.length,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3), itemBuilder: (context,index){
                return NumButton(child: NumPad[index],onTap: () => buttonPressFunction(NumPad[index]),);
            }),
            ),
            ),
        ],
      ),
    );
  }
}

class NumButton extends StatelessWidget{
  final String child;
  final VoidCallback onTap;
  var btnColor = Color.fromARGB(255, 114, 8, 96);

  NumButton({Key? key,required this.child,required this.onTap,}) :super(key: key);

  @override
  Widget build(BuildContext context){
    if(child == 'C'){
      btnColor = Color.fromARGB(255, 219, 0, 0);
    }else if(child == 'OK'){
      btnColor = Color.fromARGB(255, 38, 210, 4);
    }


    return Padding(padding: const EdgeInsets.all(4.0),
                child: GestureDetector(
                  onTap: onTap,
                  child: Container(
                    decoration: BoxDecoration(
                      color: btnColor,
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: Center(
                      child: Text(child,style: SetTextStyle,),
                    ),
                  ),
                ),
              );
  }
}

